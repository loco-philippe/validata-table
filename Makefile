.DEFAULT: help

DC_PROD := docker compose -f docker-compose.yml
DC_DEV := $(DC_PROD) -f docker-compose.dev.override.yml

.PHONY: help
help:
	@ grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## Run all the tests
	@ $(DC_DEV) run --rm ui pytest . tests

.PHONY: code_coverage
code_coverage: ## Estimate the code coverage rate of tests
	@ cd src && poetry run coverage run -m pytest . tests && poetry run coverage html

.PHONY: lint
lint: black isort flake8 ## Checks and fixes linting with black, isort and flake8

.PHONY: serve-dev
serve_dev: ## Serves application in a development environment
	@ $(DC_DEV) up --build

.PHONY: serve-prod
serve_prod: ## Serves application in a production environment (as a daemon)
	@ $(DC_PROD) up -d --build

########################################

SUCCESS_MSG :=  "\033[0;32mദി(˵•̀ ᴗ -˵)✧\033[0m"

.PHONY: black
black:
	@ echo "~~~~ Running black ~~~~"
	@ $(DC_DEV) run --rm ui poetry run black -- . && echo $(SUCCESS_MSG)
	@ echo ""

.PHONY: isort
isort:
	@ echo "~~~~ Running isort ~~~~"
	@ $(DC_DEV) run --rm ui poetry run isort . && echo $(SUCCESS_MSG)
	@ echo ""

.PHONY: flake8
flake8:
	@ echo "~~~~ Running flake8 ~~~~"
	@ $(DC_DEV) run --rm ui poetry run flake8 . && echo $(SUCCESS_MSG)
	@ echo ""

