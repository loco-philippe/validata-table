import pprint
from typing import Any, Dict, List, Optional, Union

import frictionless

from validata_core.errors import Error
from validata_core.resource import Resource
from validata_core.structure_warnings import iter_structure_warnings


class Task:
    def __init__(
        self,
        task: frictionless.ReportTask,
        resource: Optional[frictionless.Resource],
        errors: List[frictionless.Error],
        original_schema: frictionless.Schema,
    ):
        # Init Task object with frictionless.Task object's properties
        self.name = task.name
        self.type = task.type
        self.title = task.title
        self.description = task.description
        self.valid = task.valid
        self.place = task.place
        self.labels = task.labels
        self.stats = task.stats
        self.warnings = task.warnings

        # Edit Task object ihnerited frictionless.Task object's properties :
        # Edit task_stats fields with original schema data as the task is built on resource's schema which is modified by frictionless at its creation
        self.stats["fields"] = len(original_schema.fields) if original_schema else 0
        self.errors: List[Error] = [Error(err) for err in errors]

        # Add specific new attributes the Task object, most of them are deprecated
        self.frless_task: frictionless.ReportTask = task
        self.original_schema: frictionless.Schema = original_schema

        ## All following attributes are deprecated in frictionless
        ## but kept in this class for retrocompatibility
        self.resource: Optional[frictionless.Resource] = resource
        self.scope: List[str] = [
            "hash-count-error",
            "byte-count-error",
            "field-count-error",
            "row-count-error",
            "blank-header",
            "extra-label",
            "missing-label",
            "blank-label",
            "duplicate-label",
            "incorrect-label",
            "blank-row",
            "primary-key-error",
            "foreign-key-error",
            "extra-cell",
            "missing-cell",
            "type-error",
            "constraint-error",
            "unique-error",
        ]
        self.partial: bool = False
        self.structure_warnings: list[Union[Dict[str, str], Any, None]] = [
            iter_structure_warnings(warning) for warning in self.warnings
        ]
        self.time: Union[float, int] = task.stats["seconds"]

    def __repr__(self) -> str:
        """Overwrites frictionless.metadata __repr__() method to use overwritten 'to_dict()' method.
        Returns a dict representation of Task object
        """
        return pprint.pformat(self.to_dict(), sort_dicts=True)

    def to_dict(self) -> Dict[str, Any]:
        """Overwrites frictionless.metadata to_dict().
        Returns a dict containing all Task object's properties information with added
        some deprecated information linked to this task.
        """
        dict_task = self.frless_task.to_dict()
        dict_task["errors"] = [error.to_dict() for error in self.errors]

        # All following key-values are deprecated in frictionless
        # but kept in this class for retrocompatibility
        dict_task["partial"] = self.partial
        dict_task["scope"] = self.scope
        dict_task["structure_warnings"] = self.structure_warnings
        dict_task["time"] = self.time
        if self.resource:
            resource = Resource(self.resource)
            dict_task["resource"] = resource.to_dict(self.original_schema)
        return dict_task
