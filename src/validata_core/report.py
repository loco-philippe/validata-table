import pprint
from datetime import datetime, timezone
from importlib.metadata import version
from typing import Any, Dict, List

import frictionless

from validata_core.errors import Error
from validata_core.task import Task


class Report:
    date: str
    errors: List[Error]
    version: str
    tasks: List[Task]
    frless_report: frictionless.Report

    def __init__(
        self,
        report: frictionless.Report,
        errors: List[Error],
        tasks: List[Task],
    ):
        # Init Report object with frictionless.Report object's properties
        self.valid = report.valid
        self.stats = report.stats
        self.name = report.name
        self.title = report.title
        self.description = report.description
        self.warnings = report.warnings

        # Edit Report object ihnerited frictionless.Report object's properties
        self.errors = errors
        self.tasks = tasks

        # Add specific new properties the Report object
        self.frless_report = report
        self.date = datetime.now(timezone.utc).isoformat()
        self.version = version("frictionless")
        self.time = report.stats["seconds"]  # Deprecated

    def __repr__(self) -> str:
        """Overwrites frictionless.metadata __repr__ method to use overwritten 'to_dict()' method.
        Returns a dict representation of Report object.
        """
        return pprint.pformat(self.to_dict(), sort_dicts=True)

    def to_dict(self) -> Dict[str, Any]:
        """Overwrites frictionless.metadata to_dict().
        Returns a dict containing all Report object's properties information.
        """
        report_dict = self.frless_report.to_dict()
        report_dict["date"] = self.date
        report_dict["errors"] = [error.to_dict() for error in self.errors]
        report_dict["tasks"] = [task.to_dict() if task else {} for task in self.tasks]
        report_dict["time"] = self.time  # Deprecated
        report_dict["version"] = self.version

        return report_dict
