from typing import Any, Dict

import frictionless


class Resource:
    def __init__(self, resource: frictionless.Resource):
        self.frless_resource = resource

    def to_dict(self, original_schema: frictionless.Schema) -> Dict[str, Any]:
        """Create and returns a dict containing informations attributes
        of the frictionless Resource object given in parameter with added
        some deprecated information linked to this resource.
        """
        dict_resource = self.frless_resource.to_dict()

        # Edit task_resource schema with original schema as the resource's schema is modified by frictionless at its creation
        dict_resource["schema"] = original_schema.to_dict()

        # All following key-values are deprecated in frictionless
        # but kept in this class for retrocompatibility
        dict_resource["layout"] = {
            "headerCase": self.frless_resource.dialect.header_case,
            "limitRows": 100000,
        }
        dict_resource["hashing"] = "deprecated"
        dict_resource["profile"] = "deprecated"
        dict_resource["scheme"] = "deprecated"
        dict_resource["stats"] = {
            "bytes": self.frless_resource.bytes
            if self.frless_resource.bytes
            else "deprecated",
            "fields": len(original_schema.fields),
            "hash": self.frless_resource.hash
            if self.frless_resource.hash
            else "deprecated",
            "rows": self.frless_resource.stats.rows,
        }

        return dict_resource
