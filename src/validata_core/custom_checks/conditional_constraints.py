# -*- coding: utf-8 -*-
"""
Created on Thu May  9 11:50:13 2024

@author: a lab in the Air
"""

import attrs
import frictionless
import jsonschema
from frictionless import Check, Row
from frictionless.errors import RowError

keywords = ['anyOf', 'properties', 'not', 'const', 'fields', 'anyOf', 'allOf', 'oneOf', 'if', 'then', 'else', 'enum']

def validate(resource):
    checks = [Composition({key:resource.schema.custom[key]}) 
              for key in resource.schema.custom if key in ['allOf', 'anyOf', 'oneOf']]
    if 'if' in resource.schema.custom:
        checks += [Composition({key:resource.schema.custom[key] 
                               for key in resource.schema.custom 
                               if key in ['if', 'then', 'else']})]
    return frictionless.validate(resource, checks=checks)

def add_prop(json_value):
    '''add "properties" keyword for JSON Schema check'''
    if isinstance(json_value, list):
        return [add_prop(val) for val in json_value]
    if isinstance(json_value, dict) and len(json_value) > 1 :
        return {key: add_prop(val) for key, val in json_value.items()}
    if isinstance(json_value, dict) and len(json_value) == 1 :
        key_val = list(json_value)[0]
        if key_val in keywords:
            return {key: add_prop(val) for key, val in json_value.items()}
        return {'properties': {key_val: add_prop(json_value[key_val])}}
    return json_value
    
class CompositionError(RowError):
    title = None
    type = 'composition'
    description = None

@attrs.define(kw_only=True, repr=False)
class Composition(Check):
    """Check a Composition of schemas"""

    type = "composition"
    Errors = [CompositionError]

    def __init__(self, descriptor):
        super().__init__()
        self.__composition = add_prop(descriptor)
        self.__descriptor = descriptor 
        
    def validate_row(self, row: Row):        
        try:
            jsonschema.validate(row, self.__composition)
        except Exception:
            note = 'the row is not conform to schema : ' + str(self.__descriptor)[0:15] + '...'
            yield CompositionError.from_row(row, note=note)