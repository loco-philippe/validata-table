# -*- coding: utf-8 -*-
"""
Created on Thu May  9 11:48:03 2024

@author: a lab in the Air
"""
from typing import Optional, Type

import attrs
from frictionless import Check, Row
from frictionless.errors import RowError
from tab_dataset import Cdataset, Cfield
from typing_extensions import Self

class RelationshipError(RowError):
    title = None
    type = 'relationships'
    description = None
    template = "row position {rowNumber} is not consistent"

@attrs.define(kw_only=True, repr=False)
class Relationship(Check):
    """Check a Relationship between two fields"""

    type = "relationships"
    Errors = [RelationshipError]
    
    def __init__(self, **kwargs):
        super().__init__()
       
    @classmethod
    def from_descriptor(cls, descriptor, relationships=None):
        res_t = list(map(list, zip(*relationships)))
        dts = Cdataset([Cfield(fld[1:], fld[0]) for fld in res_t])
        check = super().from_descriptor(descriptor)
        check.__num_row = -1
        check.__relationship = descriptor
        check.__errors = dts.check_relation(descriptor['fields'][0],
                                          descriptor['fields'][1],
                                          descriptor['link'], value=False)        
        return check
        
    def validate_row(self, row: Row):
        self.__num_row += 1
        if self.__num_row in self.__errors:
            note = 'cells "' + self.__relationship['fields'][0] + \
                   '" and "' + self.__relationship['fields'][1] + \
                   '" are not ' + self.__relationship['link'] + ' in this row'
            yield RelationshipError.from_row(row, note=note)

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    
    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["fields", "link"],
        "properties": {
            "link": {"type": "string"},
            "fields": {"type": "array"}}
    }
