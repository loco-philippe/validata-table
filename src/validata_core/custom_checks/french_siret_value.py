from typing import Any, Generator, Optional, Type

import attrs
import frictionless
import stdnum.fr.siret
from typing_extensions import Self

from .utils import CustomCheckSingleColumn, CustomErrorSingleColumn


class FrenchSiretValueError(CustomErrorSingleColumn):
    """Custom error."""

    type = "french-siret-value"
    name = "Numéro SIRET invalide"
    title = name
    tags = ["#body"]
    template = "La valeur {cell} n'est pas un numéro SIRET français valide."
    description = (
        "Le numéro de SIRET indiqué n'est pas valide selon la définition"
        " de l'[INSEE](https://www.insee.fr/fr/metadonnees/definition/c1841)."
    )


@attrs.define(kw_only=True, repr=False)
class FrenchSiretValue(CustomCheckSingleColumn):
    """Check french SIRET number validity."""

    type = "french-siret-value"
    Errors = [FrenchSiretValueError]

    def _validate_start(self) -> Generator[Any, Any, Any]:
        yield from []

    def _validate_row(
        self, cell_value: Any, row: frictionless.Row
    ) -> Generator[FrenchSiretValueError, Any, Any]:
        if not stdnum.fr.siret.is_valid(cell_value):
            yield FrenchSiretValueError.from_row(row, note="", field_name=self.column)

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["column"],
        "properties": {"column": {"type": "string"}},
    }
