from typing import Any, Generator, Optional, Type

import attrs
import frictionless
import opening_hours
from typing_extensions import Self

from .utils import CustomCheckSingleColumn, CustomErrorSingleColumn


class OpeningHoursValueError(CustomErrorSingleColumn):
    """Custom error."""

    type = "opening-hours-value"
    name = "Horaires d'ouverture incorrects"
    title = name
    tags = ["#body"]
    template = (
        "La valeur '{cell}' n'est pas une définition d'horaire d'ouverture correcte.\n\n"
        " Celle-ci doit respecter la spécification"
        " [OpenStreetMap](https://wiki.openstreetmap.org/wiki/Key:opening_hours)"
        " de description d'horaires d'ouverture."
    )
    description = ""


@attrs.define(kw_only=True, repr=False)
class OpeningHoursValue(CustomCheckSingleColumn):
    """Check opening hours validity."""

    type = "opening-hours-value"
    Errors = [OpeningHoursValueError]

    def _validate_start(self) -> Generator[Any, Any, Any]:
        yield from []

    def _validate_row(
        self, cell_value: Any, row: frictionless.Row
    ) -> Generator[OpeningHoursValueError, Any, Any]:
        if not opening_hours.validate(cell_value):  # type: ignore
            yield OpeningHoursValueError.from_row(row, note="", field_name=self.column)

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["column"],
        "properties": {"column": {"type": "string"}},
    }
