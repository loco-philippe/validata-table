from typing import Any, Generator, Optional, Type

import attrs
import frictionless
import stdnum.fr.siren
from typing_extensions import Self

from .utils import CustomCheckSingleColumn, CustomErrorSingleColumn


class FrenchSirenValueError(CustomErrorSingleColumn):
    """Custom error."""

    type = "french-siren-value"
    name = "Numéro SIREN invalide"
    title = name
    tags = ["#body"]
    template = "La valeur {cell} n'est pas un numéro SIREN français valide."
    description = (
        "Le numéro de SIREN indiqué n'est pas valide selon la définition"
        " de l'[INSEE](https://www.insee.fr/fr/metadonnees/definition/c2047)."
    )


@attrs.define(kw_only=True, repr=False)
class FrenchSirenValue(CustomCheckSingleColumn):
    """Check french SIREN number validity."""

    Errors = [FrenchSirenValueError]

    type = "french-siren-value"

    def _validate_start(self) -> Generator[Any, Any, Any]:
        yield from []

    def _validate_row(
        self, cell_value: Any, row: frictionless.Row
    ) -> Generator[FrenchSirenValueError, Any, Any]:
        if not stdnum.fr.siren.is_valid(cell_value):
            yield FrenchSirenValueError.from_row(row, note="", field_name=self.column)

    @classmethod
    def metadata_select_class(cls, type: Optional[str]) -> Type[Self]:
        return cls

    metadata_profile = {  # type: ignore
        "type": "object",
        "required": ["column"],
        "properties": {"column": {"type": "string"}},
    }
