import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import frictionless
from frictionless import errors as frerrors
from frictionless import formats
from frictionless import resources as frresources

from .error_messages import error_translate

log = logging.Logger(__name__)


@dataclass
class ValidataSourceError(Exception):
    name: str
    message: str


@dataclass
class FileExtensionError(Exception):
    name: str
    message: str


class ValidataResource(ABC):
    """A resource to validate: url or uploaded file"""

    @abstractmethod
    def build_table_args(self) -> Tuple[Union[str, bytes], dict]:
        """return (source, option_dict)"""
        pass

    @abstractmethod
    def get_source(self) -> str:
        """return filename or URL"""
        pass

    def extract_tabular_data(self) -> Tuple[list[str], list[str]]:
        """Extract header and data rows from source."""
        table_source, table_options = self.build_table_args()
        return _extract_header_and_rows_from_frictionless_source(
            table_source, **table_options
        )

    def _control_option(self, format: str) -> dict:
        # In Frictionless v5  ExcelFormat dialect is replaced by ExcelControl format,
        # see https://framework.frictionlessdata.io/docs/formats/excel.html for more information
        return (
            {"control": formats.ExcelControl(preserve_formatting=True)}
            if format == "xlsx"
            else {}
        )

    @classmethod
    def detect_encoding(cls, buffer: bytes) -> str:
        """Try to decode using utf-8 first, fallback on frictionless helper function."""
        try:
            buffer.decode("utf-8")
            return "utf-8"
        except UnicodeDecodeError:
            encoding = frictionless.Detector().detect_encoding(buffer)
            return encoding.lower()


class URLValidataResource(ValidataResource):
    """URL resource"""

    def __init__(self, url: str):
        """Built from URL"""
        self.url = url

    def get_source(self) -> str:
        return self.url

    def build_table_args(self) -> Tuple[str, dict]:
        """URL implementation"""
        suffix = Path(self.url).suffix
        format = suffix[1:] if suffix.startswith(".") else ""

        return (
            self.url,
            {
                "detector": frictionless.Detector(
                    encoding_function=ValidataResource.detect_encoding
                ),
                **self._control_option(format),
            },
        )


class FileContentValidataResource(ValidataResource):
    """Uploaded file resource"""

    def __init__(self, filename: Optional[str], content: bytes):
        """Built from filename and bytes content"""
        if filename is None:
            self.filename = ""
            self.file_ext = Path("").suffix.lower()
        else:
            self.filename = filename
            self.file_ext = Path(filename).suffix.lower()
        self.content = content

    def get_source(self) -> str:
        return self.filename

    def build_table_args(self) -> Tuple[bytes, dict]:
        """Uploaded file implementation"""

        def detect_format_from_file_extension(file_ext: str):
            if file_ext in (".csv", ".tsv", ".ods", ".xls", ".xlsx"):
                return file_ext[1:]
            else:
                raise FileExtensionError(
                    name="file_extension_error",
                    message=f"This file extension {file_ext} is not yet supported",
                )

        format = detect_format_from_file_extension(self.file_ext)
        options = {
            "format": format,
            "detector": frictionless.Detector(
                encoding_function=ValidataResource.detect_encoding
            ),
            **self._control_option(format),
        }
        if format in {"csv", "tsv"}:
            options["encoding"] = ValidataResource.detect_encoding(self.content)
        source = self.content

        return (source, options)


def _extract_header_and_rows_from_frictionless_source(
    source, **source_options
) -> Tuple[list[str], list[Any]]:
    """Extract header and data rows from frictionless source and options."""
    try:
        with frresources.table.TableResource(source=source, **source_options) as res:
            if res.cell_stream is None:
                raise ValueError("impossible de lire le contenu")
            lines = list(res.read_cells())
            if not lines:
                raise ValueError("contenu vide")
            header = lines[0]
            rows = lines[1:]
            # Fix BOM issue on first field name
            BOM_UTF8 = "\ufeff"
            if header and header[0].startswith(BOM_UTF8):
                header = [header[0].replace(BOM_UTF8, "")] + header[1:]
            return header, rows
    except ValueError as vex:
        raise ValidataSourceError(
            name="source-error",
            message=vex.args[0],
        ) from vex
    except frictionless.exception.FrictionlessException as exc:
        validata_error = error_translate(exc.error)
        raise ValidataSourceError(
            name=validata_error.title,
            message=validata_error.message,
        ) from exc


BODY_TAGS = frozenset(["#body", "#cell", "#content", "#row", "#table"])
STRUCTURE_TAGS = frozenset(["#head", "#structure", "#header"])


def is_body_error(err: Union[frerrors.Error, Dict]) -> bool:
    """Classify the given error as 'body error' according to its tags."""
    tags = err.tags if isinstance(err, frerrors.Error) else err["tags"]
    return bool(BODY_TAGS & set(tags))


def is_structure_error(err: Union[frerrors.Error, Dict]) -> bool:
    """Classify the given error as 'structure error' according to its tags."""
    tags = err.tags if isinstance(err, frerrors.Error) else err["tags"]
    return bool(STRUCTURE_TAGS & set(tags))


def to_lower(str_array: List[str]) -> List[str]:
    """Lower all the strings in a list"""
    return [s.lower() for s in str_array]
