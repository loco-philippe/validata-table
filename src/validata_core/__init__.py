import logging
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import frictionless
from frictionless import Check, Schema
from frictionless import types as frtypes

from validata_core.custom_checks import available_checks
from validata_core.custom_checks.utils import build_check_error
from validata_core.error_messages import error_translate
from validata_core.errors import Error
from validata_core.helpers import (
    ValidataSourceError,
    _extract_header_and_rows_from_frictionless_source,
)
from validata_core.report import Report
from validata_core.task import Task
from validata_core.warning_messages import iter_warnings

log = logging.getLogger(__name__)

VALIDATA_MAX_ROWS = 100000


def extract_required_field_names(
    schema: frictionless.Schema,
) -> list[str]:
    return [
        field.name
        for field in schema.fields
        if field.constraints
        and "required" in field.constraints
        and field.constraints["required"]
    ]


def get_custom_checks_from_schema(
    schema: frictionless.Schema,
) -> List[Dict]:
    if "custom_checks" in schema.to_dict().keys():
        return schema.to_dict()["custom_checks"]
    else:
        return []


def build_validation_checks_and_check_errors_from_schema(
    schema: frictionless.Schema,
    **options
) -> Tuple[List[Check], List[frictionless.Error]]:
    """Build specific validation checks used for validate data
    from schema custom checks and constraints, and
    build related check errors.
    """

    # First check is devoted to limit amount of rows to read
    validation_checks = []
    validation_checks.append(
        frictionless.Check.from_descriptor(
            {"type": "table-dimensions", "maxRows": VALIDATA_MAX_ROWS}
        )
    )

    # Dynamically add custom check based on schema needs
    check_errors = []

    custom_checks = get_custom_checks_from_schema(schema)

    for custom_check in custom_checks:
        check_name = custom_check["name"]
        if check_name not in available_checks:
            check_errors.append(
                build_check_error(
                    check_name,
                    note=f"Tentative de définir le custom check {check_name}, qui n'est pas connu.",
                )
            )
            continue
        check_class = available_checks[check_name]
        check_descriptor = custom_check["params"]
        # added line:
        option_check = {k: v for k, v in options.items() if k == check_name}

        validation_checks.append(
            check_class.from_descriptor(descriptor=check_descriptor, **option_check)
            #check_class.from_descriptor(descriptor=check_descriptor)
        )

    return validation_checks, check_errors


def translate_errors(report: frictionless.Report, schema: frictionless.Schema):
    """
    Translate errors contained in the validation report in french langage.
    """
    report.errors = [error_translate(err, schema) for err in report.errors]
    for table in report.tasks:
        table.errors = [error_translate(err, schema) for err in table.errors]


def _create_resource(
    source: Union[str, Path, list[Any]],
    schema: Schema,
    validate_options: Dict[str, Any],
) -> frictionless.Resource:
    # Merge options to pass to frictionless

    resource = frictionless.Resource(source, schema=schema, **validate_options)
    return resource


def _create_validate_options(header_case: bool, **options) -> Dict[str, Any]:
    return {
        # Dialect replace Layout class in v5 of frictionless
        "dialect": frictionless.Dialect(header_case=header_case),
        # Don't care about missing, extra or unordered columns
        "detector": frictionless.Detector(schema_sync=True),
        **{
            k: v
            for k, v in options.items()
            if k
            in {
                "format",
                "scheme",
            }
        },
    }


def validate_schema(
    schema_descriptor: Union[frtypes.IDescriptor, str],
) -> frictionless.Report:
    schema_validation_report = frictionless.validate(
        frictionless.Schema.from_descriptor(schema_descriptor)
    )

    return schema_validation_report


def validate(
    source: Union[str, Path, list[Any]],
    schema_descriptor: Union[frtypes.IDescriptor, str],
    header_case: bool = True,
    **options,
) -> Report:
    """
    Validate a `source` using a `schema` returning a validation report.
    """

    # Handle different schema format
    try:
        schema = frictionless.Schema.from_descriptor(schema_descriptor)
    except frictionless.FrictionlessException as exception:
        errors = exception.reasons if exception.reasons else [exception.error]
        return format(frictionless.Report.from_validation(errors=errors), None, None)

    # Build checks and related errors from schema
    (
        validation_checks,
        check_errors,
    ) = build_validation_checks_and_check_errors_from_schema(schema, **options)
    #) = build_validation_checks_and_check_errors_from_schema(schema)

    validate_options = _create_validate_options(header_case, **options)

    original_schema = schema.to_copy()
    resource = _create_resource(source, schema, validate_options)
    report = frictionless.validate(
        source=resource,
        checks=validation_checks,
    )

    source_header = None
    if report.tasks:
        try:
            source_header, _ = _extract_header_and_rows_from_frictionless_source(
                source, **validate_options
            )
        except ValidataSourceError:
            source_header = None

    required_field_names = extract_required_field_names(schema)

    for table in report.tasks:
        # Add warnings
        if source_header:
            table.warnings = list(
                iter_warnings(
                    source_header, required_field_names, original_schema, header_case
                )
            )
            table.stats["warnings"] += len(table.warnings)
            report.stats["warnings"] += len(table.warnings)
            report.warnings += table.warnings

        # Add custom-checks errors
        if check_errors:
            table.errors.extend(check_errors)
            report.stats["errors"] += len(check_errors)
            table.stats["errors"] += len(check_errors)
            report.valid = False

    # Translate errors
    translate_errors(report, schema)
    formatted_report = format(report, resource, original_schema)
    return formatted_report


def format(
    report: frictionless.Report,
    resource: Optional[frictionless.Resource],
    original_schema: Optional[frictionless.Schema],
) -> Report:
    """This function aims to format the validata_core validation report
    into a dict form and transform it by adding all missing properties which
    were contained in validation report form when using frictionless v4.38.0
    but not existing using frictionless v5.16.
    These properties are added in order that Validata still is retrocompatible
    with upgrading to the v5 of frictionless.
    These added properties are now depreciated and mentionned as well in the
    returned dict. They will be removed in the future.
    """

    # Create errors from frictionless report
    formatted_errors = [Error(err) for err in report.errors]

    # Create tasks content from frictionless report
    formatted_tasks = []
    if original_schema:
        formatted_tasks = create_tasks_contents_for_formatted_report(
            report, resource, original_schema
        )

    formatted_report = Report(report, formatted_errors, formatted_tasks)

    return formatted_report


def create_tasks_contents_for_formatted_report(
    report: frictionless.Report,
    resource: Optional[frictionless.Resource],
    original_schema: frictionless.Schema,
) -> List[Task]:
    """This function aims to complete each tasks contained in the formatted report
    with all the task properties which were contained in an task report object in version
    4.38.0 of frictionless.
    """

    formatted_tasks = []

    for task in report.tasks:
        # Build task errors
        formatted_tasks.append(Task(task, resource, task.errors, original_schema))

    return formatted_tasks
