import pprint
from typing import Any, Dict, List

import frictionless
from frictionless import errors as frerrors


class Error:
    def __init__(self, error: frictionless.Error):
        self.frless_error: frictionless.Error = error

        # All following attributes are deprecated in frictionless
        # but kept in this class for retrocompatibility

        self.code: str = error.type
        self.name: str = ""

        # Init Error object with frictionless.CellError object's properties
        if isinstance(error, frerrors.CellError):
            self.field_position: int = error.field_number
            self.row_position: int = error.row_number

        # Init Error object with frictionless.LabelError object's properties
        if isinstance(error, frerrors.LabelError):
            self.field_position: int = error.field_number
            self.row_positions: List[int] = error.row_numbers

        # Init Error object with frictionless.RowError object's properties
        if isinstance(error, frerrors.RowError):
            self.row_position: int = error.row_number

    @property
    def type(self) -> str:
        return self.frless_error.type

    @property
    def title(self) -> str:
        return self.frless_error.title

    @property
    def message(self) -> str:
        return self.frless_error.message

    @property
    def note(self) -> str:
        return self.frless_error.note

    def __repr__(self) -> str:
        """Overwrites frictionless.metadata __repr__ method to use overwritten 'to_dict()' method.
        Returns a dict representation of Error object
        """
        return pprint.pformat(self.to_dict(), sort_dicts=True)

    def to_dict(self) -> Dict[str, Any]:
        """Overwrites frictionless.metadata to_dict() method.
        Returns a dict containing all Error object's properties information with added
        some deprecated information linked to this error.
        """
        dict_error = self.frless_error.to_dict()

        # All following key-values are deprecated in frictionless
        # but kept in this class for retrocompatibility
        dict_error["code"] = self.code
        dict_error["name"] = self.name

        if "fieldNumber" in dict_error.keys():
            dict_error["fieldPosition"] = dict_error["fieldNumber"]

        if "rowNumber" in dict_error.keys():
            dict_error["rowPosition"] = dict_error["rowNumber"]

        if "rowNumbers" in dict_error.keys():
            dict_error["rowPositions"] = dict_error["rowNumbers"]

        return dict_error
