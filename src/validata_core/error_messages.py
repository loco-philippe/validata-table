import re
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple, Union

import frictionless
import frictionless.errors as ferr
import frictionless.fields as frfields

DATETIME_RE = re.compile(r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$")
CONSTRAINT_RE = re.compile(r'^constraint "([^"]+)" is .*$')
ARRAY_CONSTRAINT_RE = re.compile(r'^array item constraint "([^"]+)" is .*$')


def required_constraint() -> Tuple[str, str]:
    """Return french name and french message related to
    'required' schema field constraint.
    """
    return "Cellule vide", "Une valeur doit être renseignée."


def unique_constraint() -> Tuple[str, str]:
    """Return french name and french message related to
    'unique' schema field constraint.
    """
    return (
        "Valeur déjà présente",
        "Toutes les valeurs de cette colonne doivent être unique.",
    )


def min_length_constraint(constraint_val: Any, cell_value: Any) -> Tuple[str, str]:
    """Return french name and french message related to
    'minLength' schema field constraint.
    """
    msg = (
        f"Le texte attendu doit comporter au moins {constraint_val} caractère(s)"
        f" (au lieu de {len(cell_value)} actuellement)."
    )
    return "Valeur trop courte", msg


def max_length_constraint(constraint_val: Any, cell_value: Any) -> Tuple[str, str]:
    """Return french name and french message related to
    'maxLength' schema field constraint.
    """
    msg = (
        f"Le texte attendu ne doit pas comporter plus de {constraint_val}"
        f" caractère(s) (au lieu de {len(cell_value)} actuellement)."
    )
    return "Valeur trop longue", msg


def minimum_constraint(constraint_val: Any) -> Tuple[str, str]:
    """Return french name and french message related to
    'minimum' schema field constraint.
    """
    return (
        "Valeur trop petite",
        f"La valeur attendue doit être au moins égale à {constraint_val}.",
    )


def maximum_constraint(constraint_val: Any) -> Tuple[str, str]:
    """Return french name and french message related to
    'maximum' schema field constraint.
    """
    return (
        "Valeur trop grande",
        f"La valeur attendue doit être au plus égale à {constraint_val}.",
    )


def pattern_constraint(
    field_def: Union[frictionless.Field, frfields.array.ArrayField],
) -> Tuple[str, str]:
    """Return french name and french message related to
    'pattern' schema field constraint.
    """
    info_list = []
    if field_def.description:
        info_list.append(field_def.description + "\n")
    if field_def.example:
        info_list.append(f"## Exemple(s) valide(s)\n{field_def.example}\n")
    msg = (
        "\n".join(info_list)
        if info_list
        else "*Aucune description ni exemple à afficher.*"
    )
    return "Format incorrect", msg


def enum_constraint(constraint_val: Any) -> Tuple[str, str]:
    """Return french name and french message related to
    'enum' schema field constraint.
    """
    enum_values = constraint_val
    if len(enum_values) == 1:
        return (
            "Valeur incorrecte",
            f"L'unique valeur autorisée est : {enum_values[0]}.",
        )
    else:
        md_str = "\n".join([f"- {val}" for val in enum_values])
        return (
            "Valeur incorrecte",
            f"Les seules valeurs autorisées sont :\n{md_str}",
        )


def all_constraints(
    constraint_val: Any,
    cell_value: Any,
    field_def: Union[frictionless.Field, frfields.array.ArrayField],
) -> Dict[str, Tuple[str, str]]:
    """Return a dictionnary containing all communes schema field constraints
    with their french names and messages accociated
    """
    is_enum_field = isinstance(field_def, frfields.array.ArrayField) or (
        isinstance(field_def, frfields.string.StringField)
        and "enum" in field_def.constraints.keys()
    )

    return {
        "required": required_constraint(),
        "unique": unique_constraint(),
        "minLength": min_length_constraint(constraint_val, cell_value),
        "maxLength": max_length_constraint(constraint_val, cell_value),
        "minimun": minimum_constraint(constraint_val),
        "maximum": maximum_constraint(constraint_val),
        "pattern": pattern_constraint(field_def),
        "enum": enum_constraint(constraint_val) if is_enum_field else ("", ""),
    }


def extract_constraint_value_from_field_constraints(
    field_def: Union[frictionless.Field, frfields.array.ArrayField],
    constraint_name: str,
) -> Any:
    """Extract and return constraint value from a field constraints"""
    is_array_field = isinstance(field_def, frfields.array.ArrayField)
    if not is_array_field:
        return field_def.constraints[constraint_name]
    else:
        assert isinstance(field_def, frfields.array.ArrayField)
        assert field_def.array_item is not None
        return field_def.array_item["constraints"][constraint_name]


def constraint_name_and_message(
    cell_value: Any,
    field_def: Union[frictionless.Field, frfields.array.ArrayField],
    constraint_name: str,
) -> Tuple[str, str]:
    """Return french name and message for given constraint error."""

    constraint_value = extract_constraint_value_from_field_constraints(
        field_def, constraint_name
    )

    constraints_dict = all_constraints(constraint_value, cell_value, field_def)

    if constraint_name in constraints_dict.keys():
        return constraints_dict[constraint_name]

    else:
        return (
            "Contrainte inconnue",
            f"La contrainte {constraint_name} n'est pas reconnue par Validata.",
        )


def et_join(values: List[str]) -> str:
    """french enum
    >>> et_join([])
    ''
    >>> et_join(['a'])
    'a'
    >>> et_join(['a','b'])
    'a et b'
    >>> et_join(['a','b','c'])
    'a, b et c'
    >>> et_join(['a','b','c','d','e'])
    'a, b, c, d et e'
    """
    if not values:
        return ""
    if len(values) == 1:
        return values[0]
    return " et ".join([", ".join(values[:-1]), values[-1]])


def encoding_error(err: ferr.Error) -> Tuple[str, str]:
    """Return french name and french message related to
    'encoding' frictionless error.
    """
    return (
        "Erreur d'encodage",
        f"Un problème d'encodage empêche la lecture du fichier ({err.note})",
    )


def blank_header_error(err: ferr.BlankHeaderError) -> Tuple[str, str]:
    """Return french name and french message related to
    'blanck header' frictionless error.
    """
    title = "En-tête manquant"
    if len(err.row_numbers) == 1:
        message = f"La colonne n°{err.row_numbers[0]} n'a pas d'entête."
    else:
        pos_list = ", ".join(str(err.row_numbers))
        message = f"Les colonnes n°{pos_list} n'ont pas d'entête."
    return (title, message)


def blank_row_error() -> Tuple[str, str]:
    """Return french name and french message related to
    'blanck row' frictionless error.
    """
    return ("Ligne vide", "Les lignes vides doivent être retirées de la table.")


def extra_cell_error() -> Tuple[str, str]:
    """Return french name and french message related to
    'extra cell' frictionless error.
    """
    return (
        "Valeur surnuméraire",
        "Le nombre de cellules de cette ligne excède le nombre de colonnes défini dans le schéma.",
    )


def find_field_in_schema(
    schema: Optional[frictionless.Schema], field_name: str
) -> Optional[frictionless.Field]:
    if schema:
        return next(
            (field for field in schema.fields if field.name == field_name),
            None,
        )
    else:
        return None


def date_type_error(field_value: Any) -> Tuple[str, str]:
    title = "Format de date incorrect"

    if _is_french_date_format(field_value):
        isodate = _convert_french_to_iso_date(field_value)
        message = f"La forme attendue est {isodate}."
        return (title, message)

    # Checks if date is yyyy-mm-ddThh:MM:ss
    # print('DATE TIME ? [{}]'.format(field_value))
    dm = DATETIME_RE.match(field_value)
    if dm:
        iso_date = field_value[: field_value.find("T")]
        message = f"La forme attendue est {iso_date!r}."
        return (title, message)

    # default
    message = "La date doit être écrite sous la forme `aaaa-mm-jj`."

    return (title, message)


def number_type_error(field_value: Any) -> Tuple[str, str]:
    title = "Format de nombre incorrect"
    if "," in field_value:
        en_number = field_value.replace(",", ".")
        value_str = f"«&#160;{en_number}&#160;»"
        message = f"Le séparateur décimal à utiliser est le point ({value_str})."
    else:
        message = (
            "La valeur ne doit comporter que des chiffres"
            " et le point comme séparateur décimal."
        )
    return (title, message)


def string_type_error(field_format: Any) -> Tuple[str, str]:
    title = "Format de chaîne incorrect"
    if field_format == "uri":
        message = "La valeur doit être une adresse de site ou de page internet (URL)."
    elif field_format == "email":
        message = "La valeur doit être une adresse email."
    elif field_format == "binary":
        message = "La valeur doit être une chaîne encodée en base64."
    elif field_format == "uuid":
        message = "La valeur doit être un UUID."
    else:
        message = "La valeur doit être une chaîne de caractères."

    return (title, message)


def boolean_type_error(field_def: frfields.boolean.BooleanField) -> Tuple[str, str]:
    true_values = field_def.true_values if field_def.true_values else ["true"]
    false_values = field_def.false_values if field_def.false_values else ["false"]
    true_values_str = et_join(list(map(lambda v: "`{}`".format(v), true_values)))
    false_values_str = et_join(list(map(lambda v: "`{}`".format(v), false_values)))
    title = "Valeur booléenne incorrecte"
    message = (
        f"Les valeurs acceptées sont {true_values_str} (vrai)"
        f" et {false_values_str} (faux)."
    )

    return (title, message)


def type_error(
    err: ferr.TypeError, schema: Optional[frictionless.Schema]
) -> Tuple[str, str]:
    """Return french name and french message related to
    'type' frictionless error.
    """
    field_name = err.field_name
    field_def = find_field_in_schema(schema, field_name)

    if field_def is None:
        return ("", "")

    field_format = field_def.format
    field_value = err.cell

    if isinstance(field_def, frfields.date.DateField):
        return date_type_error(field_value)

    elif isinstance(field_def, frfields.year.YearField):
        title = "Format d'année incorrect"
        message = "L'année doit être composée de 4 chiffres."

    elif isinstance(field_def, frfields.number.NumberField):
        return number_type_error(field_value)

    elif isinstance(field_def, frfields.integer.IntegerField):
        title = "Format entier incorrect"
        message = "La valeur doit être un nombre entier."

    elif isinstance(field_def, frfields.string.StringField):
        return string_type_error(field_format)

    elif isinstance(field_def, frfields.boolean.BooleanField):
        return boolean_type_error(field_def)

    else:
        title, message = ("Cette erreur ne devrait jamais arriver", "")

    return (title, message)


def constraint_error(
    err: ferr.ConstraintError, schema: Optional[frictionless.Schema]
) -> Tuple[str, str]:
    """Return french name and french message related to
    'constraint' frictionless error.
    """
    field_name = err.field_name
    if schema:
        field_def = next(
            (field for field in schema.fields if field.name == field_name),
            None,
        )
    else:
        field_def = None

    # extract violated constraint from english message
    m = CONSTRAINT_RE.match(err.note)
    if m and field_def:
        title, message = constraint_name_and_message(err.cell, field_def, m[1])
    else:
        # extract violated array constraint from english message
        m = ARRAY_CONSTRAINT_RE.match(err.note)
        if m and field_def:
            title, message = constraint_name_and_message(err.cell, field_def, m[1])
        else:
            title = "Contrainte non respectée"
            message = err.note

    return (title, message)


def missing_label_error(err: ferr.MissingLabelError) -> Tuple[str, str]:
    """Return french name and french message related to
    'missing-label' frictionless error.
    """
    field_name = err.field_name
    return (
        "Colonne obligatoire manquante",
        f"La colonne obligatoire `{field_name}` est manquante.",
    )


def all_errors(
    err: ferr.Error, schema: Optional[frictionless.Schema]
) -> Dict[str, Tuple[str, str]]:
    """Return a dictionnary containing all communes frictionless errors
    with their french names and messages accociated
    """
    return {
        "encoding-error": encoding_error(err),
        "blank-header": (
            blank_header_error(err)
            if isinstance(err, ferr.BlankHeaderError)
            else ("", "")
        ),
        "blank-row": blank_row_error(),
        "extra-cell": extra_cell_error(),
        "type-error": (
            type_error(err, schema) if isinstance(err, ferr.TypeError) else ("", "")
        ),
        "constraint-error": (
            constraint_error(err, schema)
            if isinstance(err, ferr.ConstraintError)
            else ("", "")
        ),
        "missing-label": (
            missing_label_error(err)
            if isinstance(err, ferr.MissingLabelError)
            else ("", "")
        ),
    }


def error_translate(
    err: ferr.Error, schema: Optional[frictionless.Schema] = None
) -> ferr.Error:
    """Translate and improve error message clarity.

    Update err fields:
    - title: use a french human readable name describing the error type
    - message: french error message

    And return updated error
    """

    errors_dict = all_errors(err, schema)

    if err.type in errors_dict.keys():
        err.__setattr__("title", errors_dict[err.type][0])
        err.message = errors_dict[err.type][1]
        return err
    elif (
        err.type == "error"
        and err.message == '"schema_sync" requires unique labels in the header'
    ):
        title = "Colonnes dupliquées"
        message = "Le fichier  comporte des colonnes avec le même nom. Pour valider le fichier, veuillez d'abord le corriger en mettant des \
                valeurs uniques dans son en-tête (la première ligne du fichier)."
        err.__setattr__("title", title)
        err.message = message
        return err
    else:
        return err


def _is_french_date_format(date_string: str) -> bool:
    try:
        datetime.strptime(date_string, "%d/%m/%Y")
        return True
    except ValueError:
        return False


def _convert_french_to_iso_date(date_string: str) -> str:
    """Expects date_string to be validated as a valid french date"""
    return datetime.strptime(date_string, "%d/%m/%Y").strftime("%Y-%m-%d")
