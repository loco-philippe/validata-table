import pytest

from tests.core import utils
from validata_core import validate


@pytest.fixture
def schema_nomenclature_actes_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [{"name": "acte", "title": "Acte", "type": "string"}],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


@pytest.fixture
def schema_nomenclature_actes_for_none_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {"name": "acte", "title": "Acte", "type": "string"},
        ],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


@pytest.fixture
def schema_nomenclature_actes_value_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "acte",
                "title": "Acte",
                "type": "string",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


def test_nomenclature_actes_values_valid(
    schema_nomenclature_actes_value, schema_nomenclature_actes_for_none_value
):
    test_cases = [
        {
            "source": [["acte"], ["Fonction publique/foobar"]],
            "schema": schema_nomenclature_actes_value,
        },
        {
            # Multi-case in value
            "source": [["acte"], ["fOnCtIOn pubLIQUE/foobar"]],
            "schema": schema_nomenclature_actes_value,
        },
        {
            # Ignore custom check on empty column not required
            "source": [["A", "acte"], ["a", None]],
            "schema": schema_nomenclature_actes_for_none_value,
        },
    ]
    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        assert report.valid


def test_nomenclature_actes_values_invalid(
    schema_nomenclature_actes_value,
    schema_nomenclature_actes_for_none_value,
    schema_nomenclature_actes_value_on_required_field,
):
    test_cases = [
        {
            # Empty cell on required missing column related to custom check:
            # ignore custom check, only constraint-error related to empty cell is reported
            "source": [["A", "acte"], ["a", None]],
            "schema": schema_nomenclature_actes_value_on_required_field,
            "type_error_expected": "constraint-error",
            "message_part_error_expected": "Une valeur doit être renseignée.",
        }
    ]
    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        print(report)
        utils.assert_single_error(report)
        error = utils.get_report_errors(report)[0]
        assert error.type == tc["type_error_expected"]
        assert tc["message_part_error_expected"] in error.message
