from pathlib import Path

import pytest

from tests.core import utils
from validata_core import validate


@pytest.fixture
def array_schema():
    return {
        "$schema": "https://specs.frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "array_field",
                "title": "Array field",
                "description": "Array field description",
                "type": "array",
                "arrayItem": {"constraints": {"enum": ["a", "b"]}},
            }
        ],
    }


def test_validate_ko(array_schema):
    report = validate(Path("tests/core/fixtures/invalid_array.csv"), array_schema)
    report_errors = utils.get_report_errors(report)
    assert len(report_errors) == 2
    assert report_errors[0].type == "constraint-error"
    assert report_errors[1].type == "constraint-error"
    assert not report.valid


def test_validate_ok(array_schema):
    report = validate(Path("tests/core/fixtures/valid_array.csv"), array_schema)
    assert report.valid
